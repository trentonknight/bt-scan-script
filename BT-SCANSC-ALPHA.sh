#!/bin/bash
# For ANSI and RGB colors use the following
# \033[38;2;<r>;<g>;<b>m
# echo -e "\033[38;2;215;144;21mTitle of the Program\033[0m"
echo '\xE2\x98\xA0'
echo -e "\e[0;92mflushing arp cache first ...\e[0m"
ip neigh flush any
echo -e "\033[38;2;215;144;21mrunning nmap host only discovery then using arp cache to create list.\033[0m"
#CIDRZ=$(ip route | awk '(NR>1) {print $1}')
CIDRZ=$(ip route | awk 'FNR == 2 {print $1}')
echo "${CIDRZ}"
DT=$(date +"%d-%m-%Y")
nmap -sn "${CIDRZ}" -oG nmap-"${DT}".txt
awk '!/Nmap/ {print $2}' nmap-"${DT}".txt > list-"${DT}".txt
#nmap -sn -iL list-"${DT}".txt
nmap -sC --max-hostgroup 2 -iL list-"${DT}".txt
